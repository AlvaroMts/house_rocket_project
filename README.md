# House_Rocket_Project

# 1. Business Problem

**Company Name:** House Rocket

**Product/Service:** The company buy and sell houses.

**Business Model:** The company buy and sell houses and sometimes reform them to raise the value, and their profit relies on the spread (difference in the price paid and sold of the houses)

**Problem:**
They don't have a way to visualize the data (list of houses and their features) to evaluate and identify houses with the best opportunity

# 2. Business Assumptions

**Current Situation:** To this project we are assuming 2 scenarios:

1. House Rocket is interested in buying houses and they have a dataset of houses they kept track of to indentify the best opportunities;

2. House Rocket is selling all houses of the dataset and they want to raise their profit by raising the price of the houses.

# 3. Solution Strategy

1. Collect data from Kaggle (https://www.kaggle.com/harlfoxem/housesalesprediction)

2. Data description by zipcode- Use basic statistics on the data to have a first look at the data.

3. Data Cleaning - Check NA values, Outliers, change data types

4. Group data by Zipcode

5. Elaborate 10 Hypothesis and validate them

6. Find Median price per zipcode and by trimester

7. Suggest houses with lower prices than the median on corresponding zipcode and if with good condition (4-5)

8. Assuming that the data is the portfolio of the company (the company own all the houses), i'll suggest a price target.

9. Deploy Model to Production on Streamlit. It will contain a dataframe with the dataset and a map with the location of all properties and filters to select houses.

# 4. Top 3 Insights

# 5. Business Results

**1st scenario:** Assuming that House Rocket buy all suggested houses and sell them by the mean price of the region, they could profit up to R$ 330,312,625.

**2nd scenario:** Assuming that House Rocket is going to sell all houses of the dataset, they would generate a total profit of R$ 2,252,953,241.6.

# 6. Conclusion

# 7. Lessons Learned

# 8. Next Steps
