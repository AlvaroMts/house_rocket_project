#############################
# IMPORTS
#############################

import pandas as pd
import numpy as np
import folium
from folium.plugins import MarkerCluster
import datetime
import math
import streamlit as st
from streamlit_folium import folium_static
import geopandas

#############################
# LOADING DATA
#############################
st.set_page_config(layout='wide')
@st.cache(allow_output_mutation=True)
def get_data(path):
	data = pd.read_csv(path)
	return data

@st.cache(allow_output_mutation=True)
def get_geofile(url):
	geofile = geopandas.read_file(url)
	
	return geofile
#############################
# CHANGING DATA TYPE
#############################

def change_dtypes(data):

	# date: Object -> Datetime
	data['date'] = pd.to_datetime(data['date']).dt.strftime('%Y-%m-%d')

	# Bathrooms: Float -> Int
	data['bathrooms'] = data['bathrooms'].astype('int')

	# Floors: Float -> Int
	data['floors'] = data['floors'].astype('int')
	
	return data

def data_cleaning(data):
	# droping houses with bedrooms > 10  OR bedrooms == 0 OR bathrooms == 0 
	drop = data[(data['bedrooms']>10) |
			    (data['bedrooms'] == 0) | 
			    (data['bathrooms'] == 0)].index
	data = data.drop(drop)
	
	# DROPPING UNNECESSARY COLUMNS
	data = data.drop(columns=['sqft_living15', 'sqft_lot15'])
	
	return data

def set_feature(data):
	
	# Calculate total_sqft (sqft_living + sqft_basement + sqft_above)
	data['total_sqft'] = data['sqft_living'] + data['sqft_living'] + data['sqft_above'] 
	
	# Calculating price/sqft_living
	data['price/sqft'] = data['price'] / data['total_sqft']

	# Calculating median price by zipcode
	aux = data[['zipcode', 'price/sqft']].groupby('zipcode').median().reset_index()
	aux.columns = ['zipcode', 'zipcode_median_price']

	# Merging dataframes
	data = pd.merge(data, aux, on='zipcode', how='inner')
                                
	return data
	
def select_houses(data):

	# Creating Flag Column (Buy or Don't Buy)
	data['flag'] = data.apply(lambda x: 'Buy' if (x['price/sqft'] < x['zipcode_median_price']) &
    	                                         (x['condition'] >= 4) else "Don't Buy", axis=1)
    	                                         
	data = data[data['flag']=='Buy']
	
	data['spread'] = data['zipcode_median_price'] - data['price/sqft']
    	                                         
	return data
    
def house_price_target(data):
	# Labelling the profit percentage (10% or 30%)
	data['profit_perc'] = data.apply(lambda x: '10%' if x['price/sqft'] > x['zipcode_median_price'] else '30%', axis=1)

	# Calculating the Final cost of houses
	data['price_target'] = data.apply(lambda x: (x['price']) + (x['price']*0.1) if x['profit_perc'] == '10%' else
                                            (x['price']) + (x['price']*0.3), axis=1)

	# Calculating nominal profit
	data['nominal_profit'] = data['price_target'] - data['price']
	
	return data
	
def table_filters(data):
	#data = data.drop(columns=['sqft_lot', 'yr_renovated', 'lat', 'long', 'view', 'sqft_living', 'sqft_basement', 'sqft_above'])
	st.sidebar.title('Portfolio Filters')
	
	filters = {'zipcode': st.sidebar.multiselect('Zipcode', np.sort(data['zipcode'].unique())),
			   'price': st.sidebar.select_slider('Price Range', options=list(np.sort(data['price'].unique())), value=(data['price'].min(), data['price'].max())),
			   'sqft': st.sidebar.select_slider('Square Foot Range', options=list(np.sort(data['total_sqft'].unique())), value=(data['total_sqft'].min(), data['total_sqft'].max())),
			   'bedrooms': st.sidebar.multiselect('Number of Bedrooms', np.sort(data['bedrooms'].unique())),
			   'bathrooms': st.sidebar.multiselect('Number of Bathrooms', np.sort(data['bathrooms'].unique())),
			   'waterfront': 1 if st.sidebar.checkbox('Waterfront') else 0,
			   'condition': st.sidebar.multiselect('House Condition', np.sort(data['condition'].unique())),
			   'yr_built': st.sidebar.select_slider('House Year Built', options=list(np.sort(data['yr_built'].unique())), value=(data['yr_built'].min(), data['yr_built'].max())),
			   }
			   
	data = data if filters['zipcode']==[] else data[data['zipcode'].isin(filters['zipcode'])]
	data = data[(data['price'] >= filters['price'][0]) & (data['price'] <= filters['price'][1])]
	data = data[(data['total_sqft'] >= filters['sqft'][0]) & (data['total_sqft'] <= filters['sqft'][1])]
	data = data if filters['bedrooms']==[] else data[data['bedrooms'].isin(filters['bedrooms'])]
	data = data if filters['bathrooms']==[] else data[data['bathrooms'].isin(filters['bathrooms'])]
	data = data[data['waterfront']==filters['waterfront']]
	data = data if filters['condition']==[] else data[data['condition'].isin(filters['condition'])]
	data = data[(data['yr_built'] >= filters['yr_built'][0]) & (data['yr_built'] <= filters['yr_built'][1])]
	
	return data
	

def portfolio_map(data, geofile):
	# creating map
	#data = data.sample(1000)
	m = folium.Map(location=[data['lat'].mean(),
				   data['long'].mean()],
				   default_zoom_start=15)
						 
	marker_cluster = MarkerCluster().add_to(m)
	
	for name, row in data.iterrows():
		text = 'Price = R$ {0} <br> Size = {1} sqft <br> Bedrooms = {2}<br> Bathrooms = {3}'.format(row['price'],
					  															 row['total_sqft'],
					  															 row['bedrooms'],
					  															 row['bathrooms'])
		iframe = folium.IFrame(text)
		popup = folium.Popup(iframe, min_width=180, max_width=180)
		
		folium.Marker([row['lat'], row['long']],
					  popup=(popup)).add_to(marker_cluster)
	
	folium_static(m)
	
	return None


if __name__ == '__main__':
	# ETL
	
	# ================================
	# EXTRACTION
	# ================================
	path = 'kc_house_data.csv'
	url = 'https://opendata.arcgis.com/datasets/83fc2e72903343aabff6de8cb445b81c_2.geojson'
	# Get Dataset
	data = get_data(path)
	# Get geofile
	geofile = get_geofile(url)
	
	# ================================
	# TRANSFORMATION
	# ================================
	data = data_cleaning(data)
    
	data = change_dtypes(data)
	
	data = set_feature(data)
	
	# showing portfolio

	st.title('Portfolio')
	portfolio = table_filters(data)
	st.write(portfolio)
	portfolio_map(portfolio, geofile)
	
	# selecting houses to buy and display them
	st.title('Houses to Buy')
	houses_selected = select_houses(data)
	st.write(houses_selected) 
	
	# Display on map
	
	portfolio_map(houses_selected, geofile)
	
	# Calculating Price Target to Sell
	st.title('Calculating House Price Target')
	price_target = house_price_target(data)
	st.write(price_target)


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
